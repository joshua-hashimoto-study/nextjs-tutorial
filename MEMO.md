# Memo

## Table of Content

[TOC]

---

## Routing

pages配下のファイル全ては自動的にルーティングが作成される。パスはファイルの構成通りになる。

### 動的ルーティング

ファイル/フォルダを`[]`で囲うことで動的ルーティングになる。

```jsx
<Link as="/car/bruno" href="/[vehicle]/[person]"></Link>
```

`as`が実際のURLを示していて、`href`が動的バスを示している。

---

## API

Next.jsを使ってAPIを作る時も`pages`配下に作っていく。

---
