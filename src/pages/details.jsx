import Link from 'next/link'
import { capitalize } from '../utils/capitalize';

const people = [
    {vehicle: 'car', name: 'Bruno'},
    {vehicle: 'bike', name: 'Joshua'},
    {vehicle: 'airplane', name: 'Mike'},
]

const Details = () => {
    return (
        <div>
            {people.map(person => {
                return (
                    <div key={person.vehicle}>
                        <Link as={`/${person.vehicle}/${person.name}`} href="/[vehicle]/[person]">
                            <a>Navigate to {capitalize(person.name)}'s {capitalize(person.vehicle)}</a>
                        </Link>
                    </div>
                );
            })}
        </div>
    )
}

export default Details