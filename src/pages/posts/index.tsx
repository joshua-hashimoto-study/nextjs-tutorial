import { useEffect, useState } from 'react'
import Link from 'next/link';
import { NextPage } from "next";

import { Post } from "../../models/post";


export type PostsProps = {
    posts: Post[] | undefined;
}

const Posts: NextPage<PostsProps> = ({posts}) => {
    const [postList, setPostList] = useState(posts)

    useEffect(() => {
        if (posts?.length === 0) {
            loadData();
        }
    }, [])

    const loadData = async () => {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const posts = await response.json();
        setPostList(posts);
    }

    return (
        <div>
            {postList?.map(post => (
                <div key={post.id}>
                    <Link as={`/posts/${post.id}`} href={'/posts/[id]'}>
                        <a>{post.title}</a>
                    </Link>
                </div>
            ))}
        </div>
    );
}

Posts.getInitialProps = async ({ req }) => {
    if (!req) {
        // if you are in the client(the server is not there yet),
        // return the empty list.
        // this way the navigation will start soon.
        return {posts: []}
    }
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    const posts: Post[] | undefined = await response.json();
    return {posts}
}

export default Posts;