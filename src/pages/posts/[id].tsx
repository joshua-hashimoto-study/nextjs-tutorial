import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { NextPageContext } from "next";

import { Post } from "../../models/post";

const emptyPost: Post = {userId: 0, id:0, title:'', body:''}

export type PostItemProps = {
    post: Post | undefined;
}


const fetchPost = async (postId: number) => {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`);
    const post: Post | undefined = await response.json();
    return post;
}


const PostItem = ({post}: PostItemProps) => {
    const router = useRouter();
    const [data, setData] = useState(post)

    useEffect(() => {
        if (!data?.id) {
            loadData();
        }
    }, [])

    const loadData = async () => {
        const postId = router.query.id;
        const post = await fetchPost(postId ? +postId : 0);
        setData(post)
    }

    if (!data?.id) {
        return <p>Loading...</p>
    }


    return (
        <div>
            <h2>{data?.title}</h2>
            <p>{data?.body}</p>
        </div>
    )
}

// extends but with type instead of interface
type CustomNextPageContext = NextPageContext & {
    // override the query to insure the query "id" exists and typed to number
    query : {
        id: number;
    }
}


PostItem.getInitialProps = async ({ query, req }: CustomNextPageContext) => {
    if (!req) {
        return {post: emptyPost}
    }
    // you do not need to use useRouter in getInitialProps.
    // router query is passed in by default
    const postId = query.id;
    const post: Post | undefined = await fetchPost(postId);
    return {post}
}

export default PostItem