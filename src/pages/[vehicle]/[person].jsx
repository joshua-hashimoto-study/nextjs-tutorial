import { useRouter } from 'next/router'
import { capitalize } from '../../utils/capitalize';


const Person = () => {
    const router = useRouter();

    console.log(router.query);
    // router.query contains the route including query parameters

    return <h2>{capitalize(router.query.person)}'s {capitalize(router.query.vehicle)}</h2>
}

export default Person;