import { NextApiRequest, NextApiResponse } from "next";
import sqlite from "sqlite";

const getProfiles = async (request: NextApiRequest, response: NextApiResponse) => {
    const db = await sqlite.open('./mydb.sqlite');
    const profiles = await db.all('select * from person');
    response.json(profiles);
}

export default getProfiles