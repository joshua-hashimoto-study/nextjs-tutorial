import { NextApiRequest, NextApiResponse } from "next";
import sqlite from 'sqlite';

const getPersonById = async (request: NextApiRequest, response: NextApiResponse) => {
    const db = await sqlite.open('./mydb.sqlite');

    if (request.method === 'PUT') {
        const statement = await db.prepare('UPDATE person SET name =?, email = ? where id = ?');
        const result = await statement.run(request.body.name, request.body.email, request.query.id);
        result.finalize();
    }

    // GET
    const person = await db.get('select * from person where id = ?', [request.query.id]);
    response.json(person);
}

export default getPersonById