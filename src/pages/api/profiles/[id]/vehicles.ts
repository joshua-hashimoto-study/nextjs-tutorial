import { NextApiRequest, NextApiResponse } from "next";
import sqlite from 'sqlite';

const getAllVehicleByPersonId = async (request: NextApiRequest, response: NextApiResponse) => {
    const db = await sqlite.open('./mydb.sqlite');
    const vehicles = await db.all('select * from vehicle where ownerId = ?', [request.query.id]);
    response.json(vehicles);
}

export default getAllVehicleByPersonId