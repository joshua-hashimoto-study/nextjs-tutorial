import { NextApiRequest, NextApiResponse } from "next";
import sqlite from 'sqlite';

const getVehicleById = async (request: NextApiRequest, response: NextApiResponse) => {
    const db = await sqlite.open('./mydb.sqlite');
    const vehicle = await db.get('select * from vehicle where id = ?', [request.query.id]);
    response.json(vehicle);
}

export default getVehicleById