import { NextApiRequest, NextApiResponse } from "next";
import sqlite from 'sqlite';

const getAllVehicles = async (request: NextApiRequest, response: NextApiResponse) => {
    if (request.method !== 'GET') {
        // if you want to restrict HTTP methods...
    }
    const db = await sqlite.open('./mydb.sqlite');
    const vehicles = await db.all('select * from vehicle');
    response.json(vehicles);
}

export default getAllVehicles