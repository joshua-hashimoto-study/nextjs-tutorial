export type VehicleModel = {
    id: number;
    brand: string;
    model: string;
    ownerId: number;
}